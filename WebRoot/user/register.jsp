<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en">
  <head>
  	<base href="<%=basePath%>">
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/favicon.ico">

    <title>注册</title>

    <%@ include file="../util/head.html" %>

    <!-- Custom styles for this template -->
    <link href="assets/css/register.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="assets/css/Validform.css" rel="stylesheet"/>
    <style>
    	#inputPassword{margin-bottom:-1px;}
    </style>
    <script type="text/javascript" src="assets/js/Validform_v5.3.2_min.js"></script>

  </head>

  <body>

    <div class="container">

      <form class="form-register" action="register.action" method="post">
        <h2 class="form-register-heading">注册</h2>
        <dl>
        <dd>
        	<input name="email" type="email" datatype="e" ajaxurl="util/validUser.jsp" errormsg="邮件格式不正确" sucmsg="账号验证通过" nullmsg="请填写邮箱" id="inputEmail" class="form-control" placeholder="邮箱" autofocus>
        </dd>
        <dd>
        	<div class="Validform_checktip"></div>
        </dd>
        </dl>
        
        <dl>
        <dd>
        <input name="password" type="password" datatype="*6-18" errormsg="请填写6-18位的字符" sucmsg="密码验证通过" nullmsg="请填写密码"  id="inputPassword" class="form-control" placeholder="密码">
        </dd>
        <dd>
        <div class="Validform_checktip"></div>
        </dd>
        </dl>
        
        <dl>
        <dd>
        <input type="password" id="inputPassword2" datatype="*"  recheck="password" errormsg="两次密码不一致" sucmsg="正确" nullmsg="请再次输入密码"  class="form-control" placeholder="确认密码">
        </dd>
        <dd>
        	<div class="Validform_checktip"></div>
        </dd>
        </dl>

        <dl>
        <dd>
        <button class="btn btn-lg btn-primary btn-block" type="submit">注册</button>
        </dd>
        </dl>
      </form>

    </div> <!-- /container -->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script type="text/javascript">
    	 $(function(){
	    	 $(".form-register").Validform({
	    			tiptype:2,
	    			showAllError:true
	    		});
    	 });
    </script>
  </body>
</html>

