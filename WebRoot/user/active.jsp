<%@page import="com.bean.UserInfo"%>
<%@page import="com.dao.impl.UserInfoDaoImpl"%>
<%@page import="com.dao.UserInfoDao"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>


 <%!
 void toForward(String toPageLink,String toPageName,String err,String forwardPage,HttpServletRequest request,HttpServletResponse response) throws Exception{
	request.setAttribute("err", err);
	request.setAttribute("toPageLink", toPageLink);
	request.setAttribute("toPageName", toPageName);
	RequestDispatcher rd = request.getRequestDispatcher(forwardPage);
	rd.forward(request, response);
 }
  %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>激活用户</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

  </head>
  
  <body>
<%
int uid = 0;
String ukey = "";

if(request.getParameter("uid")!=null && request.getParameter("uid")!=""){
	uid = Integer.valueOf(request.getParameter("uid"));
}
if(request.getParameter("ukey")!=null && request.getParameter("ukey")!=""){
	ukey = request.getParameter("ukey");
}

//参数为空~跳转
if(uid==0 && ukey==""){
	/* request.setAttribute("err", "错误，请检查链接完整性");
	request.setAttribute("toPageLink", "index.jsp");
	request.setAttribute("toPageName", "首页");
	RequestDispatcher rd = request.getRequestDispatcher("../error.jsp");
	rd.forward(request, response); */
	this.toForward("index.jsp", "首页", "错误，参数不能为空，请检查链接完整性","../error.jsp", request, response);
	return;
}

UserInfoDao userInfoDao = new UserInfoDaoImpl();
//检查uid和ukey是否正确
if(!userInfoDao.checkUidAndUkey(uid,ukey)){
	/* request.setAttribute("err", "错误，请检查链接完整性");
	request.setAttribute("toPageLink", "index.jsp");
	request.setAttribute("toPageName", "首页");
	RequestDispatcher rd = request.getRequestDispatcher("../error.jsp");
	rd.forward(request, response); */
	this.toForward("index.jsp", "首页", "错误，key值不正确，请检查链接完整性","../error.jsp", request, response);
	return;
}

UserInfo userInfo = userInfoDao.getUserById(uid);



//检查用户是否已激活
/* if(userInfoDao.isActive(uid)){//已激活
	this.toForward("user/login.jsp", "登录页", "用户已激活，您可以直接登录", "../error.jsp",request, response);
	return;
} */

if(userInfo.getState()==1){//已激活
	this.toForward("user/login.jsp", "登录页", "用户已激活，您可以直接登录", "../error.jsp",request, response);
	return;
}

//检查用户是否已被管理员禁用
if(userInfo.getState()==2){//已禁用
	this.toForward("user/register.jsp", "注册页", "您已被系统 管理员拉入黑名单，请联系管理员或用新邮箱注册！", "../error.jsp",request, response);
	return;
}

//以上都没问题，执行更新数据库操作
if(!userInfoDao.activeUser(uid)){
	this.toForward("index.jsp", "首页", "激活失败，请稍候重试","../error.jsp", request, response);
	return;
}else{
	//激活成功，跳转到提示页面
	this.toForward("user/login.jsp", "登录页面", "恭喜您激活成功！", "../tips.jsp", request, response);
}
%>
  </body>
</html>
