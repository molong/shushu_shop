<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

  <%
  /* 	String error = "404 您访问的页面不存在。。。";
  	String href="index.jsp";
  	String a = "首页";
  	String err = request.getParameter("err");
  	String toPageLink = request.getParameter("toPageLink");
  	String toPageName = request.getParameter("toPageName");
  	if(err!=null && err!="" && toPageLink!=null && toPageLink!="" && toPageName!=null && toPageName!=""){
  		error = err;
  		href = toPageLink;
  		a = toPageName;
  	} */
   %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>error</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="Refresh" content="2; url=${toPageLink }"/>
	<%@ include file="util/head.html" %>
  </head>

  <body class="container">
    <div class="alert alert-danger" role="alert">
		<center><h1>${err }</h1></center>
		<center><h1>正在为你跳转到<a href="${toPageLink }">${toPageName }</a></h1></center>
	</div>
  </body>
</html>
