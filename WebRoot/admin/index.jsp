<%@page import="com.bean.UserInfo"%>
<%@page import="com.dao.UserInfoDao"%>
<%@page import="com.dao.impl.UserInfoDaoImpl"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/admin/";
%>

<!DOCTYPE html>
<html lang="en">
<!-- container-fluid -->
<head>
<base href="<%=basePath%>" />
<title>Shushu Admin</title>
<meta charset="UTF-8" />
<meta http-equiv="pragma" content="no-cache">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="util/head.html"%>
</head>
<body>


	<div id="header">
		<h1>
			<a href="./dashboard.html">Unicorn Admin</a>
		</h1>
	</div>

	<div id="user-nav" class="navbar navbar-inverse">
		<ul class="nav btn-group">
			<li class="btn btn-inverse"><a title="" href="#"><i
					class="icon icon-user"></i> <span class="text">我</span></a></li>
			<li class="btn btn-inverse dropdown" id="menu-messages"><a
				href="#" data-toggle="dropdown" data-target="#menu-messages"
				class="dropdown-toggle"><i class="icon icon-envelope"></i> <span
					class="text">消息</span> <span class="label label-important">5</span>
					<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a class="sAdd" title="" href="#">new message</a></li>
					<li><a class="sInbox" title="" href="#">inbox</a></li>
					<li><a class="sOutbox" title="" href="#">outbox</a></li>
					<li><a class="sTrash" title="" href="#">trash</a></li>
				</ul></li>
			<li class="btn btn-inverse"><a title="" href="#"><i
					class="icon icon-cog"></i> <span class="text">设置</span></a></li>
			<li class="btn btn-inverse"><a title="" href="../logout.action"><i
					class="icon icon-share-alt"></i> <span class="text">登出</span></a></li>
		</ul>
	</div>

	<div id="sidebar">
		<a href="#" class="visible-phone"><i class="icon icon-home"></i>
			控制面板</a>
		<ul>
			<li class="active"><a href="index.jsp"><i
					class="icon icon-user"></i> <span>用户管理</span></a></li>
			<li class="submenu"><a href="#"><i class="icon icon-th-list"></i>
					<span>商品管理</span></a>
				<ul>
					<li><a href="goodsmrg.jsp"><i class="icon icon-barcode"></i>
							商品管理</a></li>
					<li><a href="categorymrg.jsp"><i
							class="icon icon-list-alt"></i> 分类目录</a></li>
				</ul></li>
			<li><a href="ordermrg.jsp"><i class="icon icon-tint"></i> <span>订单管理</span></a></li>
			<li><a href="storage_outmrg.jsp"><i class="icon icon-lock"></i>
					<span>待出库商品</span></a></li>

		</ul>

	</div>

	<div id="style-switcher">
		<i class="icon-arrow-left icon-white"></i> <span>Style:</span> <a
			href="#grey" style="background-color: #555555;border-color: #aaaaaa;"></a>
		<a href="#blue" style="background-color: #2D2F57;"></a> <a href="#red"
			style="background-color: #673232;"></a>
	</div>

	<div id="content">
		<div id="content-header">
			<h1>用户管理</h1>
		</div>


		<div id="breadcrumb">
			<a href="#" title="首页" class="tip-bottom"><i class="icon-home"></i>
				控制面板</a> <a href="#" class="tip-bottom">用户管理</a>
		</div>

		<div class="container-fluid">
			<div class="row-fluid">
			
<%
/*分页数据*/
	int totalCounts = 0;//所有数据条数
	int pageCounts = 0;//总页数
	int pageCount = 10;//每页显示的数量
	int pageIndex = 1;//当前页
	
	UserInfoDao userInfoDao = new UserInfoDaoImpl();
	totalCounts = userInfoDao.getTotalCounts();
	pageCounts = totalCounts%pageCount==0?totalCounts/pageCount:totalCounts/pageCount+1;
	if(request.getParameter("pageIndex")!=null && request.getParameter("pageIndex")!=""){
		pageIndex = Integer.valueOf(request.getParameter("pageIndex"));
	}
	
	//根据分页查询用户信息
	List<UserInfo> userInfos = userInfoDao.getUserInfoFoPaging(pageCount,pageIndex);
	pageContext.setAttribute("userInfos", userInfos);
 %>
			
				<div class="widget-box">
					<div class="widget-title">

						<h5>用户列表</h5>
					</div>
					<div class="widget-content nopadding">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
							role="grid">
							<table class="table table-bordered data-table dataTable"
								id="DataTables_Table_0">
								<thead>
									<tr role="row">
										<th>邮箱</th>
										<th>手机</th>
										<th>角色</th>
										<th>用户状态</th>
										<th>注册时间</th>
										<th>操作</th>
									</tr>
								</thead>

								<tbody role="alert" aria-live="polite" aria-relevant="all">
									<c:forEach items="${userInfos }" var="userInfo" varStatus="status">
										<tr class="gradeA 
											<c:if test="${status.index%2==0}">odd</c:if>
											<c:if test="${status.index%2!=0}">even</c:if>
										">
											<td class="center sorting_1">${userInfo.email }</td>
											<td class="center ">${userInfo.tel }</td>
											<td class="center ">
												<c:if test="${userInfo.actor==1}">管理员</c:if>
												<c:if test="${userInfo.actor==2}">普通用户</c:if>
												<input type="hidden" name="actor_${userInfo.id }" value="${userInfo.actor }"/>
											</td>
											<td class="center">
												<c:if test="${userInfo.state==0}">未激活</c:if>
												<c:if test="${userInfo.state==1}">正常</c:if>
												<c:if test="${userInfo.state==2}">禁用</c:if>
												<input type="hidden" name="state_${userInfo.id }" value="${userInfo.state }"/>
											</td>
											<td class="center ">${userInfo.createtime }</td>
											
											<td class="center">
												<button class="btn btn-info btn-sm">编辑</button>
												<button <c:if test="${userInfo.state!=1}">onclick="activeUser('${userInfo.id}');"</c:if>
												  class="btn btn-success <c:if test="${userInfo.state==1}">disabled</c:if>">激活</button>
												<button <c:if test="${userInfo.state!=2}">onclick="denyUser('${userInfo.id}');"</c:if>
												  class="btn btn-warning <c:if test="${userInfo.state==2}">disabled</c:if>">禁用</button>
												<button onclick="delUser('${userInfo.id}');" class="btn btn-danger btn-sm" >删除</button>
											</td>
											
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<div
								class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
								<div class="dataTables_filter" id="DataTables_Table_0_filter">
									<label>Search: <input type="text"
										aria-controls="DataTables_Table_0"></label>
								</div>
								<div
									class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers"
									id="DataTables_Table_0_paginate">
									<a tabindex="0" href="index.jsp?pageIndex=1"
										class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default <%=pageIndex==1?"ui-state-disabled":"" %>"
										id="DataTables_Table_0_first">首页</a>
									<a tabindex="0" href="index.jsp?pageIndex=<%=pageIndex>1?pageIndex-1:1 %>"
										class="previous fg-button ui-button ui-state-default  <%=pageIndex==1?"ui-state-disabled":"" %>"
										id="DataTables_Table_0_previous">上一页</a>
									<span>
									<%for(int i=1;i<=pageCounts;i++){ %>
										<a tabindex="0" href="index.jsp?pageIndex=<%=i %>" class="fg-button ui-button ui-state-default <%=i==pageIndex?"ui-state-disabled":"" %>"><%=i %></a>
									<% }%>
									</span>
									<a tabindex="0" href="index.jsp?pageIndex=<%=pageIndex<pageCounts?pageIndex+1:pageCounts %>" 
										class="next fg-button ui-button <%=pageIndex==pageCounts?"ui-state-disabled":"" %>"
										id="DataTables_Table_0_next">下一页</a>
									<a tabindex="0"  href="index.jsp?pageIndex=<%=pageCounts %>"
										class="last ui-corner-tr ui-corner-br fg-button ui-button  <%=pageIndex==pageCounts?"ui-state-disabled":"" %>"
										id="DataTables_Table_0_last">末页</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- row-fluid -->
		</div>
	</div>

<!-- loading img -->
<div class="loading_img" hidden="true">
<img src="../assets/images/loadingjj.gif" />
</div>

	<%@ include file="util/foot.html"%>
	<script type="text/javascript">
	
	/*删除用户*/
		function delUser(id){
			swal({
				title: "您确定要删除吗?",
				text: "删除后此用户将永久从数据库中删除!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '删除!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				      //$(".loading_img").show();
				    	$.post(
				    	"util/ajax/user/delUser.jsp",
				    	{uid:id},
				    	function(data){
				    		var rs = eval(data);
				    		 //坑啊！json返回对象一定得双引号
				      		 if(rs.info=="true"){
				      			//$(".loading_img").hide();
				      			//swal("删除成功!", "此用户在已经飞升啦!", "success");
				      			window.location="index.jsp?pageIndex=<%=pageIndex%>"
				      		}else{
				      			swal("禁用失败!", "服务器开小差了!", "info");
				      		} 
				    	},
				    	"json"
				    	);
				    } /* else {
				      swal("取消成功", "该用户感谢您的不杀之恩 :)", "error");
				    } */
				});
			},1000);
		}
		
		/*禁用用户*/
		function denyUser(id){
			swal({
				title: "您确定要禁用此用户吗?",
				text: "这可比删除还狠啦，他将不能进行任何操作，重新注册都不行，等于废了!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '禁用!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				    	$.post(
				    	"util/ajax/user/denyUser.jsp",
				    	{uid:id},
				    	function(data){
				    		var rs = eval(data);
				    		 //坑啊！json返回对象一定得双引号
				      		 if(rs.info=="true"){
				      			//swal("禁用成功!", "此用户已被关进黑屋子!", "success");
				      			window.location="index.jsp?pageIndex=<%=pageIndex%>"
				      		}else{
				      			swal("禁用失败!", "服务器开小差了!", "info");
				      		} 
				    	},
				    	"json"
				    	);
				    } /* else {
				      swal("取消成功", "该用户感谢您的不杀之恩 :)", "error");
				    } */
			    },1000);
			});
		}
		
		
		/*激活用户*/
		function activeUser(id){
	    	swal({
				title: "您确定要激活此用户吗?",
				text: "此用户会给您献上丰厚的大礼的!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '激活!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				    	$.post(
				    	"util/ajax/user/activeUser.jsp",
				    	{uid:id},
				    	function(data){
				    		var rs = eval(data);
				      		if(rs.info=="true"){
				      			window.location="index.jsp?pageIndex=<%=pageIndex%>"
				      		}else{
				      			swal("激活失败!", "服务器开小差了!", "info");
				      		} 
				    	},
				    	"json"
				    	);
				    }
			    },1000);
			});
		
		}
	</script>
</body>
</html>
