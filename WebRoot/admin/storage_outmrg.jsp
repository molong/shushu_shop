<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/admin/";
%>

<!DOCTYPE html>
<html lang="en">
	<!-- container-fluid -->
	<head>
		<base href="<%=basePath %>" />
		<title>Shushu Admin</title>
		<meta charset="UTF-8" />
		<meta http-equiv="pragma" content="no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="util/head.html" %>
		</head>
	<body>
		
		
		<div id="header">
			<h1><a href="./dashboard.html">Unicorn Admin</a></h1>		
		</div>
		
		<div id="user-nav" class="navbar navbar-inverse">
            <ul class="nav btn-group">
                <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-user"></i> <span class="text">我</span></a></li>
                <li class="btn btn-inverse dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">消息</span> <span class="label label-important">5</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a class="sAdd" title="" href="#">new message</a></li>
                        <li><a class="sInbox" title="" href="#">inbox</a></li>
                        <li><a class="sOutbox" title="" href="#">outbox</a></li>
                        <li><a class="sTrash" title="" href="#">trash</a></li>
                    </ul>
                </li>
                <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
                <li class="btn btn-inverse"><a title="" href="../logout.action"><i class="icon icon-share-alt"></i> <span class="text">登出</span></a></li>
            </ul>
        </div>
            
		<div id="sidebar">
			<a href="#" class="visible-phone"><i class="icon icon-home"></i> 控制面板</a>
			<ul>
				<li><a href="index.jsp"><i class="icon icon-user"></i> <span>用户管理</span></a></li>
				<li class="submenu">
					<a href="#"><i class="icon icon-th-list"></i> <span>商品管理</span></a>
					<ul>
						<li><a href="goodsmrg.jsp"><i class="icon icon-barcode"></i> 商品管理</a></li>
						<li><a href="categorymrg.jsp"><i class="icon icon-list-alt"></i> 分类目录</a></li>
					</ul>
				</li>
				<li><a href="ordermrg.jsp"><i class="icon icon-tint"></i> <span>订单管理</span></a></li>
				<li class="active"><a href="storage_outmrg.jsp"><i class="icon icon-lock"></i> <span>待出库商品</span></a></li>
				
			</ul>
		
		</div>
		
		<div id="style-switcher">
			<i class="icon-arrow-left icon-white"></i>
			<span>Style:</span>
			<a href="#grey" style="background-color: #555555;border-color: #aaaaaa;"></a>
			<a href="#blue" style="background-color: #2D2F57;"></a>
			<a href="#red" style="background-color: #673232;"></a>
		</div>
		
		<div id="content">
			<div id="content-header">
				<h1>待出库商品</h1>
			</div>
			
					
			<div id="breadcrumb">
				<a href="#" title="首页" class="tip-bottom"><i class="icon-home"></i> 控制面板</a>
				<a href="#" class="tip-bottom">待出库商品</a>
			</div>
			
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="jpg-404">
						<img src="../assets/images/404.jpg" />
					</div>
				</div>
			</div>
		</div>
		
	<%@ include file="util/foot.html" %>
	</body>
</html>
