<%@page import="com.bean.GoodsType"%>
<%@page import="com.dao.impl.GoodsTypeDaoImpl"%>
<%@page import="com.dao.GoodsTypeDao"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/admin/";
%>

<!DOCTYPE html>
<html lang="en">
	<!-- container-fluid -->
	<head>
		<base href="<%=basePath %>" />
		<title>Shushu Admin</title>
		<meta charset="UTF-8" />
		<meta http-equiv="pragma" content="no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="util/head.html" %>
		<link href="../assets/css/Validform.css" rel="stylesheet"/>
		</head>
	<body>
		
		
		<div id="header">
			<h1><a href="index.html">控制面板</a></h1>		
		</div>
		
		<div id="user-nav" class="navbar navbar-inverse">
            <ul class="nav btn-group">
                <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-user"></i> <span class="text">我</span></a></li>
                <li class="btn btn-inverse dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">消息</span> <span class="label label-important">5</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a class="sAdd" title="" href="#">new message</a></li>
                        <li><a class="sInbox" title="" href="#">inbox</a></li>
                        <li><a class="sOutbox" title="" href="#">outbox</a></li>
                        <li><a class="sTrash" title="" href="#">trash</a></li>
                    </ul>
                </li>
                <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
                <li class="btn btn-inverse"><a title="" href="../logout.action"><i class="icon icon-share-alt"></i> <span class="text">登出</span></a></li>
            </ul>
        </div>
            
		<div id="sidebar">
			<a href="#" class="visible-phone"><i class="icon icon-home"></i> 控制面板</a>
			<ul>
				<li><a href="index.jsp"><i class="icon icon-user"></i> <span>用户管理</span></a></li>
				<li class="submenu active open">
					<a href="#"><i class="icon icon-th-list"></i> <span>商品管理</span></a>
					<ul>
						<li><a href="goodsmrg.jsp"><i class="icon icon-barcode"></i> 商品管理</a></li>
						<li class="active"><a href="categorymrg.jsp"><i class="icon icon-list-alt"></i> 分类目录</a></li>
					</ul>
				</li>
				<li><a href="ordermrg.jsp"><i class="icon icon-tint"></i> <span>订单管理</span></a></li>
				<li><a href="storage_outmrg.jsp"><i class="icon icon-lock"></i> <span>待出库商品</span></a></li>
				
			</ul>
		
		</div>
		
		<div id="style-switcher">
			<i class="icon-arrow-left icon-white"></i>
			<span>Style:</span>
			<a href="#grey" style="background-color: #555555;border-color: #aaaaaa;"></a>
			<a href="#blue" style="background-color: #2D2F57;"></a>
			<a href="#red" style="background-color: #673232;"></a>
		</div>
		
		<div id="content">
			<div id="content-header">
				<h1>分类管理</h1>
				
				<div class="right-div">
				<form action="addGoodsType.action" method="post" id="quickAddForm">
					<div class = "quickInput">
						<input type="text" name="name" placeholder="请输入分类名"  datatype="*" nullmsg="请填写分类名称"/>
						<input type="submit" class="btn btn-inverse" value="快速添加分类">
					</div>
				</dl>
				</form>
					
				</div>
			</div>
			
					
			<div id="breadcrumb">
				<a href="#" title="首页" class="tip-bottom"><i class="icon-home"></i> 控制面板</a>
				<a href="#" class="tip-bottom">分类管理</a>
			</div>
			
			<div class="container-fluid">
				<div class="row-fluid">
				
<%
GoodsTypeDao goodsTypeDao = new GoodsTypeDaoImpl();
List<GoodsType> goodsTypes = goodsTypeDao.getAllGoodsType();
pageContext.setAttribute("goodsTypes", goodsTypes);
 %>
					<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5>分类目录</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>分类名</th>
											<th>创建时间</th>
											<th>状态</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
									<c:forEach items="${goodsTypes }" var="goodsType" varStatus="status">
										<tr>
											<td class="center" style="width:25%">${goodsType.name}</td>
											<td class="center" style="width:25%">${goodsType.createtime}</td>
											<td class="center" style="width:25%">
												<c:if test="${goodsType.state==1}">正常</c:if>
												<c:if test="${goodsType.state==2}">禁用</c:if>
											</td>
											<td class="center">
												<button data-remodal-target="editGTModal" onclick="setEditGTModalVal('${goodsType.id}','${goodsType.name}');" class="btn btn-info btn-sm">编辑</button>
												<button <c:if test="${goodsType.state!=1 }">onclick="activeGoodsType('${goodsType.id}');"</c:if> 
												  class="btn btn-success <c:if test="${goodsType.state==1 }">disabled</c:if>">激活</button>
												<button <c:if test="${goodsType.state!=2 }">onclick="denyGoodsType('${goodsType.id}');"</c:if> 
												  class="btn btn-warning <c:if test="${goodsType.state==2 }">disabled</c:if> ">禁用</button>
												<button onclick="delGoodsType('${goodsType.id}');" 
												class="btn btn-danger btn-sm" >删除</button>
											</td>
										</tr>
									</c:forEach>
									</tbody>
								</table>							
							</div>
						</div>
				</div><!-- row-fluid -->
			</div>
		</div>
		
		<!-- 编辑商品分类信息模态框 -->
		<div class="remodal" data-remodal-id="editGTModal">
		  <button data-remodal-action="close" class="remodal-close"></button>
		  <h1>修改商品分类名称</h1>
		  	<form action="updateGoodsTypeName.action" method="post">
		  <div style="margin-top:28px;">
		  		<dl>
		  			<dd>商品分类名称：</dd>
		  			<dd><input type="text" name="gt_name"/></dd>
		  			<input type="hidden" name="gt_id"/>
		  		</dl>
		  </div>
		  <button data-remodal-action="cancel" class="remodal-cancel">取消</button>
		  <input type="submit" class="remodal-confirm" value="修改" />
		  	</form>
		</div>
		
	<%@ include file="util/foot.html" %>
    <script type="text/javascript" src="../assets/js/Validform_v5.3.2_min.js"></script>
	<script type="text/javascript">

		/*禁用分类*/
		function denyGoodsType(id){
	    	swal({
				title: "您确定要禁用此分类吗?",
				text: "禁用后此分类下的商品都将不能用!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '禁用!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				    	$.post(
				    	"util/ajax/goodstype/denyGoodsType.jsp",
				    	{gtid:id},
				    	function(data){
				    		var rs = eval(data);
				      		if(rs.info=="true"){
				      			window.location="categorymrg.jsp"
				      		}else{
				      			swal("激活失败!", "服务器开小差了!", "info");
				      		}
				    	},
				    	"json"
				    	);
				    }
			    },1000);
			});
		}
		
		/*激活分类*/
		function activeGoodsType(id){
	    	swal({
				title: "您确定要激活此分类吗?",
				text: "激活后此分类下的商品才能正常使用!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '激活!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				    	$.post(
				    	"util/ajax/goodstype/activeGoodsType.jsp",
				    	{gtid:id},
				    	function(data){
				    		var rs = eval(data);
				      		if(rs.info=="true"){
				      			window.location="categorymrg.jsp"
				      		}else{
				      			swal("激活失败!", "服务器开小差了!", "info");
				      		} 
				    	},
				    	"json"
				    	);
				    }
			    },1000);
			});
		
		}
		
		/*删除分类*/
		function delGoodsType(id){
	    	swal({
				title: "您确定要删除此分类吗?",
				text: "删除后您将看不到失去此分类以及此分类下的所有信息!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '删除!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				    	$.post(
				    	"util/ajax/goodstype/delGoodsType.jsp",
				    	{gtid:id},
				    	function(data){
				    		var rs = eval(data);
				      		if(rs.info=="true"){
				      			window.location="categorymrg.jsp"
				      		}else{
				      			swal("激活失败!", "服务器开小差了!", "info");
				      		} 
				    	},
				    	"json"
				    	);
				    }
			    },1000);
			});
		
		}
		
		/*设置修改模态框表单值*/
		function setEditGTModalVal(gtid,gtname){
			$(".remodal input[name=gt_id]").val(gtid);
			$(".remodal input[name=gt_name]").val(gtname);
		}
		
		var inst = $('[data-remodal-id=editGTModal]').remodal({
			//closeOnOutsideClick:false
		});
		
		
		$(function(){
	    	 $("#quickAddForm").Validform({
    			tiptype:3,
    			tipSweep:true,
    			showAllError:true
	    	});
    	 });
    	 
    	 
	</script>
	</body>
</html>
