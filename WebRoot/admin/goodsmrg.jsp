<%@page import="com.bean.GoodsType"%>
<%@page import="com.dao.impl.GoodsTypeDaoImpl"%>
<%@page import="com.dao.GoodsTypeDao"%>
<%@page import="com.bean.Goods"%>
<%@page import="com.dao.impl.GoodsDaoImpl"%>
<%@page import="com.dao.GoodsDao"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/admin/";
%>
								
<%
	/*分页数据*/
	int totalCounts = 0;//所有数据条数
	int pageCounts = 0;//总页数
	int pageCount = 10;//每页显示的数量
	int pageIndex = 1;//当前页
	int t_id = 0;//分页id
	
	if(request.getParameter("pageIndex")!=null && request.getParameter("pageIndex")!=""){
		pageIndex = Integer.valueOf(request.getParameter("pageIndex"));
	}
	if(request.getParameter("t_id")!=null && request.getParameter("t_id")!=""){
		t_id = Integer.valueOf(request.getParameter("t_id"));
	}
	
	pageContext.setAttribute("type_id", t_id);
	
	GoodsDao goodsDao = new GoodsDaoImpl();
	totalCounts = goodsDao.getTotalCountsByGoodsType(t_id);
	pageCounts = totalCounts%pageCount==0?totalCounts/pageCount:totalCounts/pageCount+1;
	//根据分页查询用户信息
	List<Goods> goodses = goodsDao.getGoodsForPagingByGoodsType(pageCount,pageIndex,t_id);
	pageContext.setAttribute("goodses", goodses);
%>
<!DOCTYPE html>
<html lang="en">
	<!-- container-fluid -->
	<head>
		<base href="<%=basePath %>" />
		<title>Shushu Admin</title>
		<meta charset="UTF-8" />
		<meta http-equiv="pragma" content="no-cache">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="util/head.html" %>
		</head>
	<body>
		
		
		<div id="header">
			<h1><a href="./dashboard.html">Unicorn Admin</a></h1>		
		</div>
		
		<div id="user-nav" class="navbar navbar-inverse">
            <ul class="nav btn-group">
                <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-user"></i> <span class="text">我</span></a></li>
                <li class="btn btn-inverse dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">消息</span> <span class="label label-important">5</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a class="sAdd" title="" href="#">new message</a></li>
                        <li><a class="sInbox" title="" href="#">inbox</a></li>
                        <li><a class="sOutbox" title="" href="#">outbox</a></li>
                        <li><a class="sTrash" title="" href="#">trash</a></li>
                    </ul>
                </li>
                <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
                <li class="btn btn-inverse"><a title="" href="../logout.action"><i class="icon icon-share-alt"></i> <span class="text">登出</span></a></li>
            </ul>
        </div>
            
		<div id="sidebar">
			<a href="#" class="visible-phone"><i class="icon icon-home"></i> 控制面板</a>
			<ul>
				<li><a href="index.jsp"><i class="icon icon-user"></i> <span>用户管理</span></a></li>
				<li class="submenu active open">
					<a href="#"><i class="icon icon-th-list"></i> <span>商品管理</span></a>
					<ul>
						<li class="active"><a href="goodsmrg.jsp"><i class="icon icon-barcode"></i> 商品管理</a></li>
						<li><a href="categorymrg.jsp"><i class="icon icon-list-alt"></i> 分类目录</a></li>
					</ul>
				</li>
				<li><a href="ordermrg.jsp"><i class="icon icon-tint"></i> <span>订单管理</span></a></li>
				<li><a href="storage_outmrg.jsp"><i class="icon icon-lock"></i> <span>待出库商品</span></a></li>
				
			</ul>
		
		</div>
		
		<div id="style-switcher">
			<i class="icon-arrow-left icon-white"></i>
			<span>Style:</span>
			<a href="#grey" style="background-color: #555555;border-color: #aaaaaa;"></a>
			<a href="#blue" style="background-color: #2D2F57;"></a>
			<a href="#red" style="background-color: #673232;"></a>
		</div>
		
		<div id="content">
			<div id="content-header">
				<h1>商品管理</h1>
			</div>
			
					
			<div id="breadcrumb">
				<a href="#" title="首页" class="tip-bottom"><i class="icon-home"></i> 控制面板</a>
				<a href="#" class="tip-bottom">商品管理</a>
			</div>
			
<%
	GoodsTypeDao goodsTypeDao = new GoodsTypeDaoImpl();
	List<GoodsType> goodsTypes = goodsTypeDao.getAllActiveGoodsType();
	pageContext.setAttribute("goodsTypes", goodsTypes);
 %>
			<!-- 分类标签 -->
			<div class="container-fluid category-btn-list">
				<a class="btn btn-mini  <c:if test="${0==type_id}">btn-inverse</c:if>
				" href="goodsmrg.jsp">所有分类</a>
			<c:forEach items="${goodsTypes }" var="goodsType" varStatus="status">
				<a class="btn btn-mini <c:if test="${goodsType.id==type_id}">btn-inverse</c:if>
				" href="goodsmrg.jsp?pageIndex=1&t_id=${goodsType.id }">${goodsType.name }</a>
			</c:forEach>
			</div>
			
			<div class="container-fluid">
				<div class="row-fluid">
					

			
				<div class="widget-box">
					<div class="widget-title">

						<h5>用户列表</h5>
					</div>
					<div class="widget-content nopadding">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper"
							role="grid">
							<table class="table table-bordered data-table dataTable"
								id="DataTables_Table_0">
								<thead>
									<tr role="row">
										<th>名称</th>
										<th>分类</th>
										<th>原价</th>
										<th>现价</th>
										<th>颜色</th>
										<th>创建时间</th>
										<th>状态</th>
										<th>备注</th>
										<th>操作</th>
									</tr>
								</thead>

								<tbody role="alert" aria-live="polite" aria-relevant="all">
									<c:forEach items="${goodses }" var="goods" varStatus="status">
										<tr class="gradeA 
											<c:if test="${status.index%2==0}">odd</c:if>
											<c:if test="${status.index%2!=0}">even</c:if>
										">
										<!-- 名称 -->
											<td class="center">${goods.name }</td>
										<!-- 分类 -->
											<td class="center">${goods.goodsType.name }</td>
										<!-- 原价 -->
											<td class="center">${goods.oldprice }</td>
										<!-- 现价 -->
											<td class="center">${goods.newprice }</td>
										<!-- 颜色 -->
											<td class="center">${goods.color }</td>
										<!-- 创建时间 -->
											<td class="center">${goods.createtime }</td>
										<!-- 状态 -->
											<td class="center">
												<c:if test="${goods.state==1}">正常</c:if>
												<c:if test="${goods.state==2}">下架</c:if>
											</td>
										<!-- 备注 -->
											<td class="center">${goods.text }</td>
										<!-- 操作 -->
											<td class="center">
												<button class="btn btn-info btn-sm">编辑</button>
												<button <c:if test="${goods.state!=1}">onclick="activeGoods('${goods.id}');"</c:if>
												  class="btn btn-success <c:if test="${goods.state==1}">disabled</c:if>">上架</button>
												<button <c:if test="${goods.state!=2}">onclick="denyGoods('${goods.id}');"</c:if>
												  class="btn btn-warning <c:if test="${goods.state==2}">disabled</c:if>">下架</button>
												<button onclick="delGoods('${goods.id}');" class="btn btn-danger btn-sm" >删除</button>
											</td>
											
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<div
								class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
								<div class="dataTables_filter" id="DataTables_Table_0_filter">
									<label>Search: <input type="text"
										aria-controls="DataTables_Table_0"></label>
								</div>
								<div
									class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers"
									id="DataTables_Table_0_paginate">
									<a tabindex="0" href="goodsmrg.jsp?pageIndex=1"
										class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default <%=pageIndex==1?"ui-state-disabled":"" %>"
										id="DataTables_Table_0_first">首页</a>
									<a tabindex="0" href="goodsmrg.jsp?pageIndex=<%=pageIndex>1?pageIndex-1:1 %>"
										class="previous fg-button ui-button ui-state-default  <%=pageIndex==1?"ui-state-disabled":"" %>"
										id="DataTables_Table_0_previous">上一页</a>
									<span>
									<%for(int i=1;i<=pageCounts;i++){ %>
										<a tabindex="0" href="goodsmrg.jsp?pageIndex=<%=i %>" class="fg-button ui-button ui-state-default <%=i==pageIndex?"ui-state-disabled":"" %>"><%=i %></a>
									<% }%>
									</span>
									<a tabindex="0" href="goodsmrg.jsp?pageIndex=<%=pageIndex<pageCounts?pageIndex+1:pageCounts %>" 
										class="next fg-button ui-button <%=pageIndex==pageCounts?"ui-state-disabled":"" %>"
										id="DataTables_Table_0_next">下一页</a>
									<a tabindex="0"  href="goodsmrg.jsp?pageIndex=<%=pageCounts %>"
										class="last ui-corner-tr ui-corner-br fg-button ui-button  <%=pageIndex==pageCounts?"ui-state-disabled":"" %>"
										id="DataTables_Table_0_last">末页</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			
					
				</div>
			</div>
		</div>
		
	<%@ include file="util/foot.html" %>
		<script type="text/javascript">

		/*下架商品*/
		function denyGoods(id){
	    	swal({
				title: "您确定要下架此商品吗?",
				text: "下架后商品将不能被购买，也不再显示!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '下架!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				    	$.post(
				    	"util/ajax/goods/denyGoods.jsp",
				    	{gid:id},
				    	function(data){
				    		var rs = eval(data);
				      		if(rs.info=="true"){
				      			window.location="goodsmrg.jsp?t_id=<%=t_id%>&pageIndex=<%=pageIndex%>";
				      		}else{
				      			swal("激活失败!", "服务器开小差了!", "info");
				      		} 
				    	},
				    	"json"
				    	);
				    }
			    },1000);
			});
		
		}
		
		/*上架商品*/
		function activeGoods(id){
	    	swal({
				title: "您确定要上架此商品吗?",
				text: "上架后此商品可以正常使用!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '上架!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				    	$.post(
				    	"util/ajax/goods/activeGoods.jsp",
				    	{gid:id},
				    	function(data){
				    		var rs = eval(data);
				      		if(rs.info=="true"){
				      			window.location="goodsmrg.jsp?t_id=<%=t_id%>&pageIndex=<%=pageIndex%>"
				      		}else{
				      			swal("激活失败!", "服务器开小差了!", "info");
				      		} 
				    	},
				    	"json"
				    	);
				    }
			    },1000);
			});
		
		}
		
		/*删除商品*/
		function delGoods(id){
	    	swal({
				title: "您确定要删除此商品吗?",
				text: "删除后此商品将消失!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: '删除!',
				cancelButtonText: "点错了,取消",
				closeOnConfirm: false,
				closeOnCancel: true,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				setTimeout(function(){
				    if (isConfirm){
				    	$.post(
				    	"util/ajax/goods/delyGoods.jsp",
				    	{gid:id},
				    	function(data){
				    		var rs = eval(data);
				      		if(rs.info=="true"){
				      			window.location="goodsmrg.jsp?t_id=<%=t_id%>&pageIndex=<%=pageIndex%>"
				      		}else{
				      			swal("激活失败!", "服务器开小差了!", "info");
				      		} 
				    	},
				    	"json"
				    	);
				    }
			    },1000);
			});
		
		}
    	 
	</script>
	</body>
</html>
