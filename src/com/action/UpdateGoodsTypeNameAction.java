package com.action;

import com.dao.GoodsTypeDao;
import com.dao.impl.GoodsTypeDaoImpl;

public class UpdateGoodsTypeNameAction {

	private String gt_id = "";
	private String gt_name = "";
	
	//错误参数
	private String err="";
	private String toPageLink = "";
	private String toPageName = "";
	
	public String execute(){
		GoodsTypeDao goodsTypeDao = new GoodsTypeDaoImpl();
		if(goodsTypeDao.updateGoodsTypeNameById(Integer.valueOf(gt_id),gt_name)){
			return "success";
		}else{
			this.err = "服务器开小差了，请稍后重试！";
			this.toPageLink = "admin/categorymrg.jsp";
			this.toPageName = "商品分类页";
			return "error";
		}
	}

	

	public String getGt_id() {
		return gt_id;
	}



	public void setGt_id(String gt_id) {
		this.gt_id = gt_id;
	}



	public String getGt_name() {
		return gt_name;
	}



	public void setGt_name(String gt_name) {
		this.gt_name = gt_name;
	}



	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}

	public String getToPageLink() {
		return toPageLink;
	}

	public void setToPageLink(String toPageLink) {
		this.toPageLink = toPageLink;
	}

	public String getToPageName() {
		return toPageName;
	}

	public void setToPageName(String toPageName) {
		this.toPageName = toPageName;
	}
}
