package com.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.bean.UserInfo;
import com.dao.UserInfoDao;
import com.dao.impl.UserInfoDaoImpl;
import com.util.EmailUtils;
import com.util.Secret;

public class RegisterAction {
	//?err="邮件发送失败，请重试！"&toPageLink="user/register.jsp"&toPageName="注册页 "
	private String email;
	private String password;
	//错误参数
	private String err="";
	private String toPageLink = "";
	private String toPageName = "";
	
	
	public String execute(){
		UserInfoDao userInfoDao = new UserInfoDaoImpl();
		//验证用户是否已存在~Exist
		List<UserInfo> UserInfos= userInfoDao.getUserInfoByEmail(email);
		if(UserInfos.size()>0){//如果用户已存在，返回用户已存在错误
			this.err = "用户已存在！";
			this.toPageLink = "user/register.jsp";
			this.toPageName = "注册页面";
			return "error";
		}
		//插入数据~Data
		UserInfo userInfo = new UserInfo();
		userInfo.setEmail(email);
		String new_password = Secret.MD5(password);//对密码进行加密
		userInfo.setPassword(new_password);
		String ukey = Secret.generateKey();//生成用户的随机key值
		userInfo.setUkey(ukey);
		if(!userInfoDao.addUser(userInfo)){
			this.err = "注册失败，请重试！";
			this.toPageLink = "user/register.jsp";
			this.toPageName = "注册页面";
			return "error";
		}
		
		//发送邮件~Email
		String subTitle = "shushu_shop用户注册激活邮件";
		String toEmail = this.email;
		//查id
		int uid = 0;
		uid = userInfoDao.getIdByEmail(email);
		
		String link = "localhost:8080/shushu_shop/user/active.jsp?uid="+uid+"&ukey="+ukey;
		String href = "<a href='http://"+link+"'>"+link+"</a><br>";
		String content = "感谢您加入shushu_shop，以下为您的用户激活链接<br>"+href+"<br>如不能点击，请手动复制链接到地址栏。<br>本邮件为系统自动发送，请勿回复！";
		boolean isSendEmail = EmailUtils.sendEmail(subTitle, toEmail, content);
		if(!isSendEmail){
			userInfoDao.deleteUserByEmail(email);//如果发送失败则删除刚刚插入库中的数据
			this.err = "邮件发送失败！";
			this.toPageLink = "user/register.jsp";
			this.toPageName = "注册页面";
			return "error";
		}
		//以上步骤都成功，返回success
		this.err = "恭喜您注册成功！请登录您的注册邮箱激活用户！";
		this.toPageLink = "user/login.jsp";
		this.toPageName = "登录页面";
		return "success";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}

	public String getToPageLink() {
		return toPageLink;
	}

	public void setToPageLink(String toPageLink) {
		this.toPageLink = toPageLink;
	}

	public String getToPageName() {
		return toPageName;
	}

	public void setToPageName(String toPageName) {
		this.toPageName = toPageName;
	}
	
}
