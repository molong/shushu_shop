package com.action;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.UserInfo;
import com.dao.UserInfoDao;
import com.dao.impl.UserInfoDaoImpl;
import com.util.Secret;

public class LoginAction {
	
	private String email="";
	private String password = "";

	//错误参数
	private String err="";
	private String toPageLink = "";
	private String toPageName = "";
	
	public String execute(){
		UserInfoDao userInfoDao = new UserInfoDaoImpl();
		//通过邮箱密码获取用户信息
		List<UserInfo> userInfos = userInfoDao.getUserByEmailAndPassword(this.email,this.password);
		if(userInfos.size()<1){
			this.err = "用户名或密码错误！";
			this.toPageLink = "user/login.jsp";
			this.toPageName = "登录页面";
			return "error";
		}
		//用户未激活，返回error
		Iterator<UserInfo> itr = userInfos.iterator();
		UserInfo userInfo = itr.next();
		if(userInfo.getState()==0){
			this.err = "用户未激活，请查看您的注册邮箱！";
			this.toPageLink = "user/login.jsp";
			this.toPageName = "登录页面";
			return "error";
		}
		
		//用户被禁用，返回error
		if(userInfo.getState()==2){
			this.err = "用户被禁用，请联系系统管理员！";
			this.toPageLink = "user/login.jsp";
			this.toPageName = "登录页面";
			return "error";
		}
		
		//到这一步登录成功了,将登录用户放入session
		HttpSession session = ServletActionContext.getRequest().getSession();
		session.setAttribute("userInfo", userInfo);
		
		//判断用户角色
		if(userInfo.getActor()==1){
			return "success1";
		}else{
			return "success2";
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = Secret.MD5(password);
	}

	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}

	public String getToPageLink() {
		return toPageLink;
	}

	public void setToPageLink(String toPageLink) {
		this.toPageLink = toPageLink;
	}

	public String getToPageName() {
		return toPageName;
	}

	public void setToPageName(String toPageName) {
		this.toPageName = toPageName;
	}
}
