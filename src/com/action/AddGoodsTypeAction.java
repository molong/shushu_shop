package com.action;

import com.bean.GoodsType;
import com.dao.GoodsTypeDao;
import com.dao.impl.GoodsTypeDaoImpl;
import com.util.DateUtil;

public class AddGoodsTypeAction {
	
	private String name="";

	//错误参数
	private String err="";
	private String toPageLink = "";
	private String toPageName = "";
	
	public String execute(){
		if(this.name==""){
			this.err = "请输入要添加的分类名！";
			this.toPageLink = "admin/categorymrg.jsp";
			this.toPageName = "商品分类页";
			return "error";
		}
		GoodsTypeDao goodsTypeDao = new GoodsTypeDaoImpl();
		GoodsType goodsType = new GoodsType();
		goodsType.setName(name);
		goodsType.setCreatetime(DateUtil.getCurrentTimestamp());
		goodsType.setState(1);
		if(goodsTypeDao.addGoodsType(goodsType)){
			return "success";
		}else{
			this.err = "服务器开小差了，请稍后重试！";
			this.toPageLink = "admin/categorymrg.jsp";
			this.toPageName = "商品分类页";
			return "error";
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}

	public String getToPageLink() {
		return toPageLink;
	}

	public void setToPageLink(String toPageLink) {
		this.toPageLink = toPageLink;
	}

	public String getToPageName() {
		return toPageName;
	}

	public void setToPageName(String toPageName) {
		this.toPageName = toPageName;
	}
}
