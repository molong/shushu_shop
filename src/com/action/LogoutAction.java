package com.action;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.bean.UserInfo;
import com.opensymphony.xwork2.ActionSupport;

public class LogoutAction extends ActionSupport{
	
	public String execute(){
		HttpSession session = ServletActionContext.getRequest().getSession();
		if(session.getAttribute("userInfo")!=null){
			session.removeAttribute("userInfo");
		}
		return "logout";
	}
}
