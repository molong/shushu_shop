package com.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;

import com.bean.UserInfo;

public class AdminFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		String path = ((HttpServletRequest)arg0).getContextPath();
		String basePath = arg0.getScheme()+"://"+arg0.getServerName()+":"+arg0.getServerPort()+path+"/";
		HttpSession session = ((HttpServletRequest)arg0).getSession();
		if(session.getAttribute("userInfo")!=null){
			UserInfo userInfo = (UserInfo)session.getAttribute("userInfo");
			if(userInfo.getActor()==1){
				arg2.doFilter(arg0, arg1);//
				return;
			}
		}
		//非管理员用户，say bye-bye
		((HttpServletResponse)arg1).sendRedirect(basePath+"index.jsp");
		arg2.doFilter(arg0, arg1);

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		 

	}

}
