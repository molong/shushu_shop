package com.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 * 发送邮件工具类
 */
public class EmailUtils {

	
	/**
	 * 发送邮件
	 * @param subTitle	发送标题
	 * @param toEmail	邮件接受邮箱地址
	 * @param content	发送内容	(支持html)
	 * @return		true 成功    false 失败
	 */
	public static boolean sendEmail(String subTitle,String toEmail,String content) {
		boolean fig = true;
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.auth", "true"); // 验证方式
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.port", "25"); // 端口号
		properties.setProperty("mail.smtp.host", "smtp.163.com"); // 发送邮件的邮件服务器
		Session session = Session.getInstance(properties, new Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				// 发送邮件的账号和密码
				return new javax.mail.PasswordAuthentication(
						"china_shushu@163.com", "wmy2xn14iqsfp");
			}
		});
		// 显示调试模式
		session.setDebug(true);
		// 邮件发送的消息类
		Message message = new MimeMessage(session);
		// 邮件的内容
		try {
			message.setContent(content,"text/html;charset=utf-8");
			message.setFrom(new InternetAddress("china_shushu@163.com"));
			// 邮件的标题
			message.setSubject(subTitle);
			// 接收邮件的用户
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(toEmail));
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
			fig = false;
		}
		return fig;
	}
}
