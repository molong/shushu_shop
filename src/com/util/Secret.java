package com.util;

import java.security.MessageDigest;

public class Secret {
	
	//md5加密
	public static String MD5(String str){
		try{
			MessageDigest md5 = null;
			try{
				md5 = MessageDigest.getInstance("MD5");
			}catch(Exception ex){
				System.out.println(ex.toString());
				ex.printStackTrace();
				return "";
			}
			char[] charArray = str.toCharArray();
			byte[] byteArray = new byte[charArray.length];
			
			for(int i=0;i<charArray.length;i++){
				byteArray[i] = (byte)charArray[i];
			}
			byte[] md5Bytes = md5.digest(byteArray);
			StringBuffer hexValue = new StringBuffer();
			for(int i=0;i<md5Bytes.length;i++){
				int val = (int)md5Bytes[i]&0xff;
				if(val<16){
					hexValue.append("0");
				}
				hexValue.append(Integer.toHexString(val));
			}
			return hexValue.toString();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return "";
	}
	
	//生成key
	public static String generateKey(){
		String rskey = "";
		//时间戳
		String timeStr = System.currentTimeMillis()+"";
		//随机数
		int num = (int)Math.random()*100;
		//md5
		rskey = MD5(timeStr+num);
		return rskey;
	}
}
