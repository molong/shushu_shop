package com.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;


public class DateUtil {
	
	//时间转字符串
	public static String dateToStr(Date date){
		String str = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:m:s");
		str = sdf.format(date);
		return str;
	}
	
	//字符串转时间Date类型
	public static Date strToDate(String str){
		Date d = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:m:s");
		try{
			d = sdf.parse(str);
		}catch(ParseException ex){
			ex.printStackTrace();
		}
		return d;
	}
	
	//得到当前时间戳
	public static Timestamp getCurrentTimestamp(){
		return new Timestamp(new Date().getTime());
	}
	
	//时间转时间戳
	public static Timestamp dateToTimestamp(Date d){
		return new Timestamp(d.getTime());
	}
	
	//字符串转时间戳
	public static Timestamp strToTimestamp(String str){
		return new Timestamp(strToDate(str).getTime());
	}
}
