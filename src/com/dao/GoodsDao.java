package com.dao;

import java.util.List;

import com.bean.Goods;

public interface GoodsDao {
	public int getTotalCountsByGoodsType(int t_id);//通过分类得到该类所有商品记录条数
	public List<Goods> getGoodsForPagingByGoodsType(int pageCount, int pageIndex,int t_id);//通过分类id查询该类所有商品信息
	public boolean denyGoodsById(int id);//通过id下架商品
	public boolean activeGoodsById(int id);//通过id上架商品
	public boolean delGoodsById(int id);//通过id删除商品~逻辑删除
}
