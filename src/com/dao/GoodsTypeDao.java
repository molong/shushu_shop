package com.dao;

import java.util.List;

import com.bean.GoodsType;

public interface GoodsTypeDao {
	public List<GoodsType> getAllGoodsType();//查所有商品类型
	public List<GoodsType> getAllActiveGoodsType();//查所有正常状态下的商品类型
	public boolean denyGoodsTypeById(int id);//通过id禁用分类
	public boolean activeGoodsTypeById(int id);//通过id激活分类
	public boolean delGoodsTypeById(int id);//通过id删除分类~逻辑删除
	public boolean addGoodsType(GoodsType goodsType);//添加分类
	public boolean updateGoodsTypeNameById(int id,String name);//通过id更新分类名称
}
