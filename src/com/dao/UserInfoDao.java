package com.dao;

import java.util.List;

import com.bean.UserInfo;

public interface UserInfoDao {
	public List<UserInfo> getUserInfoByEmail(String email);//根据email获取用户信息
	public boolean addUser(UserInfo userInfo);//添加用户
	public int getIdByEmail(String email);//根据email获取id
	public boolean deleteUserByEmail(String email);//根据email删除用户
	public boolean checkUidAndUkey(int uid,String ukey);//检查uid和ukey的正确性
	public boolean isActive(int uid);//检查用户是否已激活
	public boolean activeUser(int uid);//激活用户
	public List<UserInfo> getUserByEmailAndPassword(String email,String password);//根据email和密码获取用户
	public int getTotalCounts();//获得UserInfo表中的所有数据条数
	public List<UserInfo> getUserInfoFoPaging(int pageCount,int pageIndex);//使用分页查询用户信息~pageCount:总页数,pageIndex:当前页
	public boolean delUserById(int id);//通过id删除用户
	public boolean denyUserById(int id);//通过id禁用用户
	public UserInfo getUserById(int id);//通过id获得用户
}
