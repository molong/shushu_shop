package com.dao;

import org.hibernate.Session;

import factory.HibernateSessionFactory;

public class BaseDao {
	public Session getSession(){
		return HibernateSessionFactory.getSessionFactory().getCurrentSession();
	}
}
