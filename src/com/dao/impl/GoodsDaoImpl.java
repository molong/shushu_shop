package com.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bean.Goods;
import com.bean.GoodsType;
import com.dao.BaseDao;
import com.dao.GoodsDao;

public class GoodsDaoImpl extends BaseDao implements GoodsDao {

	/**
	 * 所有商品记录条数 by t_id
	 */
	@Override
	public int getTotalCountsByGoodsType(int t_id) {
		int count = 0;
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "from Goods where state!=0 and goodsType.state=1 ";//and goodsType.state==1 
			if(t_id!=0){
				hql += " and goodsType=:goodsType";
			}
			Query query = session.createQuery(hql);
			if(t_id!=0){
				GoodsType goodsType = (GoodsType)session.get(GoodsType.class, t_id);
				query.setParameter("goodsType", goodsType);
			}
			count = query.list().size();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return count;
	}

	@Override
	public List<Goods> getGoodsForPagingByGoodsType(int pageCount, int pageIndex,int t_id) {
		List<Goods> goodses = new ArrayList<Goods>();
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "from Goods where state!=0 and goodsType.state=1 ";
			if(t_id!=0){
				hql += " and goodsType.id=:t_id";
			}
			Query query = session.createQuery(hql);
			if(t_id!=0){
				//GoodsType goodsType = (GoodsType)session.get(GoodsType.class, t_id);
				//query.setParameter("goodsType", goodsType);
				query.setInteger("t_id", t_id);
			}
			query.setMaxResults(pageCount);//设置每页显示多少个，设置多大结果。
			int startC = pageCount*(pageIndex-1);
			query.setFirstResult(startC);//设置查询起点
			goodses = query.list();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return goodses;
	}

	/**
	 * 下架商品
	 */
	@Override
	public boolean denyGoodsById(int id) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			Goods goods = (Goods)session.get(Goods.class, id);
			goods.setState(2);//2表示商品被下架
			session.update(goods);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	/**
	 * 上架商品
	 */
	@Override
	public boolean activeGoodsById(int id) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			Goods goods = (Goods)session.get(Goods.class, id);
			goods.setState(1);//2表示商品状态正常
			session.update(goods);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	/**
	 * 删除商品~逻辑删除，更改state属性
	 */
	@Override
	public boolean delGoodsById(int id) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			Goods goods = (Goods)session.get(Goods.class, id);
			goods.setState(0);//2表示商品被删除
			session.update(goods);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

}
