package com.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bean.UserInfo;
import com.dao.BaseDao;
import com.dao.UserInfoDao;
import com.util.DateUtil;

public class UserInfoDaoImpl extends BaseDao implements UserInfoDao {

	@Override
	public List<UserInfo> getUserInfoByEmail(String email) {
		List<UserInfo> users = new ArrayList<UserInfo>();
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			
			String hql = "from UserInfo where email=:email";
			Query query = session.createQuery(hql);
			query.setString("email", email);
			users = query.list();
			
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return users;
	}

	@Override
	public boolean addUser(UserInfo userInfo) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			//添加默认时间
			userInfo.setCreatetime(DateUtil.getCurrentTimestamp());
			//添加默认用户状态~未激活
			userInfo.setState(0);
			//添加默认用户角色~普通用户
			userInfo.setActor(2);
			session.save(userInfo);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	@Override
	public int getIdByEmail(String email) {
		int id = -1;
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "select id from UserInfo where email=:email";
			Query query = session.createQuery(hql);
			query.setString("email", email);
			id = (Integer)query.uniqueResult();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return 0;
		}
		return id;
	}

	@Override
	public boolean deleteUserByEmail(String email) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "delete from UserInfo where email=:email";
			Query query = session.createQuery(hql);
			query.setString("email", email);
			query.executeUpdate();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	@Override
	public boolean checkUidAndUkey(int uid, String ukey) {
		Session session = null;
		Transaction tr = null;
		List<UserInfo> users = new ArrayList<UserInfo>();
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "from UserInfo where id=:id and ukey=:ukey";
			Query query = session.createQuery(hql);
			query.setInteger("id", uid);
			query.setString("ukey", ukey);
			users = query.list();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return users.size()>0?true:false;
	}

	@Override
	public boolean isActive(int uid) {
		Session session = null;
		Transaction tr = null;
		int active = 0;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "select state from UserInfo where id=:id";
			Query query = session.createQuery(hql);
			query.setInteger("id", uid);
			active = (Integer)query.uniqueResult();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return active==1?true:false;
	}

	@Override
	public boolean activeUser(int uid) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			UserInfo user = (UserInfo)session.load(UserInfo.class, uid);
			user.setState(1);
			session.update(user);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	@Override
	public List<UserInfo> getUserByEmailAndPassword(String email,
			String password) {
		List<UserInfo> userInfos = new ArrayList<UserInfo>();
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "from UserInfo where email=:email and password=:password";
			Query query = session.createQuery(hql);
			query.setString("email",email);
			query.setString("password", password);
			userInfos = query.list();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return userInfos;
	}

	@Override
	public int getTotalCounts() {
		int count = 0;
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "from UserInfo";
			Query query = session.createQuery(hql);
			count = query.list().size();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return count;
	}

	@Override
	public List<UserInfo> getUserInfoFoPaging(int pageCount, int pageIndex) {
		List<UserInfo> userInfos = new ArrayList<UserInfo>();
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "from UserInfo";
			Query query = session.createQuery(hql);
			query.setMaxResults(pageCount);//设置每页显示多少个，设置多大结果。
			int startC = pageCount*(pageIndex-1);
			query.setFirstResult(startC);//设置查询起点
			userInfos = query.list();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return userInfos;
	}

	@Override
	public boolean delUserById(int id) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			UserInfo userInfo = (UserInfo)session.get(UserInfo.class, id);
			session.delete(userInfo);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	@Override
	public boolean denyUserById(int id) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			UserInfo userInfo = (UserInfo)session.get(UserInfo.class, id);
			userInfo.setState(2);//2表示用户被禁用
			session.update(userInfo);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	@Override
	public UserInfo getUserById(int id) {
		UserInfo userInfo = null;
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			userInfo = (UserInfo)session.get(UserInfo.class, id);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return userInfo;
	}

}
