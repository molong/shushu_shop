package com.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bean.GoodsType;
import com.dao.BaseDao;
import com.dao.GoodsTypeDao;

public class GoodsTypeDaoImpl extends BaseDao implements GoodsTypeDao {

	/**
	 * 查所有商品分类信息
	 */
	@Override
	public List<GoodsType> getAllGoodsType() {
		List<GoodsType> goodsTypes = new ArrayList<GoodsType>();
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "from GoodsType where state!=:state";
			Query query = session.createQuery(hql);
			query.setInteger("state", 0);
			goodsTypes = query.list();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return goodsTypes;
	}

	/**
	 * 通过id禁用分类
	 */
	@Override
	public boolean denyGoodsTypeById(int id) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			GoodsType goodsType = (GoodsType)session.get(GoodsType.class, id);
			goodsType.setState(2);
			session.update(goodsType);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	/**
	 * 通过id激活分类
	 */
	@Override
	public boolean activeGoodsTypeById(int id) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			GoodsType goodsType = (GoodsType)session.get(GoodsType.class, id);
			goodsType.setState(1);
			session.update(goodsType);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	/**
	 * 通过id删除分类~逻辑删除，更改state属性
	 */
	@Override
	public boolean delGoodsTypeById(int id) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			GoodsType goodsType = (GoodsType)session.get(GoodsType.class, id);
			goodsType.setState(0);
			session.update(goodsType);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	/**
	 * 添加商品分类
	 */
	@Override
	public boolean addGoodsType(GoodsType goodsType) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			session.save(goodsType);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	/**
	 * 更新分类名称
	 */
	@Override
	public boolean updateGoodsTypeNameById(int id, String name) {
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			GoodsType goodsType = (GoodsType)session.get(GoodsType.class, id);
			goodsType.setName(name);
			session.update(goodsType);
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
			return false;
		}
		return true;
	}

	@Override
	public List<GoodsType> getAllActiveGoodsType() {
		List<GoodsType> goodsTypes = new ArrayList<GoodsType>();
		Session session = null;
		Transaction tr = null;
		try{
			session = getSession();
			tr = session.beginTransaction();
			String hql = "from GoodsType where state=1";
			Query query = session.createQuery(hql);
			goodsTypes = query.list();
			tr.commit();
		}catch(Exception ex){
			ex.printStackTrace();
			tr.rollback();
		}
		return goodsTypes;
	}

}
