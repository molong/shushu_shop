package com.bean;

/**
 * Order entity. @author MyEclipse Persistence Tools
 */

public class Order implements java.io.Serializable {

	// Fields

	private OrderId id;
	private Goods goods;
	private UserInfo userInfo;

	// Constructors

	/** default constructor */
	public Order() {
	}

	/** full constructor */
	public Order(OrderId id, Goods goods, UserInfo userInfo) {
		this.id = id;
		this.goods = goods;
		this.userInfo = userInfo;
	}

	// Property accessors

	public OrderId getId() {
		return this.id;
	}

	public void setId(OrderId id) {
		this.id = id;
	}

	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public UserInfo getUserInfo() {
		return this.userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

}