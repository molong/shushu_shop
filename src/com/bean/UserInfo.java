package com.bean;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * UserInfo entity. @author MyEclipse Persistence Tools
 */

public class UserInfo implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String password;
	private String tel;
	private Integer actor;
	private String email;
	private Timestamp createtime;
	private Integer state;
	private String ukey;
	private Set orders = new HashSet(0);
	private Set carts = new HashSet(0);

	// Constructors

	/** default constructor */
	public UserInfo() {
	}

	/** minimal constructor */
	public UserInfo(Integer id, String password, String email, String ukey) {
		this.id = id;
		this.password = password;
		this.email = email;
		this.ukey = ukey;
	}

	/** full constructor */
	public UserInfo(Integer id, String name, String password, String tel,
			Integer actor, String email, Timestamp createtime, Integer state,
			String ukey, Set orders, Set carts) {
		this.id = id;
		this.name = name;
		this.password = password;
		this.tel = tel;
		this.actor = actor;
		this.email = email;
		this.createtime = createtime;
		this.state = state;
		this.ukey = ukey;
		this.orders = orders;
		this.carts = carts;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Integer getActor() {
		return this.actor;
	}

	public void setActor(Integer actor) {
		this.actor = actor;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getUkey() {
		return this.ukey;
	}

	public void setUkey(String ukey) {
		this.ukey = ukey;
	}

	public Set getOrders() {
		return this.orders;
	}

	public void setOrders(Set orders) {
		this.orders = orders;
	}

	public Set getCarts() {
		return this.carts;
	}

	public void setCarts(Set carts) {
		this.carts = carts;
	}

}