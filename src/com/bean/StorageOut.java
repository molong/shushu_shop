package com.bean;

import java.sql.Timestamp;

/**
 * StorageOut entity. @author MyEclipse Persistence Tools
 */

public class StorageOut implements java.io.Serializable {

	// Fields

	private Integer id;
	private Goods goods;
	private Integer quality;
	private Timestamp updatetime;
	private Integer state;

	// Constructors

	/** default constructor */
	public StorageOut() {
	}

	/** minimal constructor */
	public StorageOut(Integer id, Goods goods, Integer quality, Integer state) {
		this.id = id;
		this.goods = goods;
		this.quality = quality;
		this.state = state;
	}

	/** full constructor */
	public StorageOut(Integer id, Goods goods, Integer quality,
			Timestamp updatetime, Integer state) {
		this.id = id;
		this.goods = goods;
		this.quality = quality;
		this.updatetime = updatetime;
		this.state = state;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public Integer getQuality() {
		return this.quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public Timestamp getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

}