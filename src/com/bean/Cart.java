package com.bean;

import java.sql.Timestamp;

/**
 * Cart entity. @author MyEclipse Persistence Tools
 */

public class Cart implements java.io.Serializable {

	// Fields

	private Integer id;
	private Goods goods;
	private UserInfo userInfo;
	private Integer quality;
	private Timestamp createtime;

	// Constructors

	/** default constructor */
	public Cart() {
	}

	/** minimal constructor */
	public Cart(Integer id, Goods goods, UserInfo userInfo, Integer quality) {
		this.id = id;
		this.goods = goods;
		this.userInfo = userInfo;
		this.quality = quality;
	}

	/** full constructor */
	public Cart(Integer id, Goods goods, UserInfo userInfo, Integer quality,
			Timestamp createtime) {
		this.id = id;
		this.goods = goods;
		this.userInfo = userInfo;
		this.quality = quality;
		this.createtime = createtime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public UserInfo getUserInfo() {
		return this.userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public Integer getQuality() {
		return this.quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public Timestamp getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}

}