package com.bean;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Goods entity. @author MyEclipse Persistence Tools
 */

public class Goods implements java.io.Serializable {

	// Fields

	private Integer id;
	private GoodsType goodsType;
	private String name;
	private String image;
	private Double oldprice;
	private Double newprice;
	private String color;
	private Timestamp createtime;
	private Integer state;
	private String text;
	private Set storages = new HashSet(0);
	private Set carts = new HashSet(0);
	private Set storageOuts = new HashSet(0);
	private Set orders = new HashSet(0);

	// Constructors

	/** default constructor */
	public Goods() {
	}

	/** minimal constructor */
	public Goods(GoodsType goodsType, String name, Double oldprice) {
		this.goodsType = goodsType;
		this.name = name;
		this.oldprice = oldprice;
	}

	/** full constructor */
	public Goods(GoodsType goodsType, String name, String image,
			Double oldprice, Double newprice, String color,
			Timestamp createtime, Integer state, String text, Set storages,
			Set carts, Set storageOuts, Set orders) {
		this.goodsType = goodsType;
		this.name = name;
		this.image = image;
		this.oldprice = oldprice;
		this.newprice = newprice;
		this.color = color;
		this.createtime = createtime;
		this.state = state;
		this.text = text;
		this.storages = storages;
		this.carts = carts;
		this.storageOuts = storageOuts;
		this.orders = orders;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public GoodsType getGoodsType() {
		return this.goodsType;
	}

	public void setGoodsType(GoodsType goodsType) {
		this.goodsType = goodsType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Double getOldprice() {
		return this.oldprice;
	}

	public void setOldprice(Double oldprice) {
		this.oldprice = oldprice;
	}

	public Double getNewprice() {
		return this.newprice;
	}

	public void setNewprice(Double newprice) {
		this.newprice = newprice;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Timestamp getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Set getStorages() {
		return this.storages;
	}

	public void setStorages(Set storages) {
		this.storages = storages;
	}

	public Set getCarts() {
		return this.carts;
	}

	public void setCarts(Set carts) {
		this.carts = carts;
	}

	public Set getStorageOuts() {
		return this.storageOuts;
	}

	public void setStorageOuts(Set storageOuts) {
		this.storageOuts = storageOuts;
	}

	public Set getOrders() {
		return this.orders;
	}

	public void setOrders(Set orders) {
		this.orders = orders;
	}

}