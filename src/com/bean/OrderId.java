package com.bean;

import java.sql.Timestamp;

/**
 * OrderId entity. @author MyEclipse Persistence Tools
 */

public class OrderId implements java.io.Serializable {

	// Fields

	private Integer id;
	private Goods goods;
	private UserInfo userInfo;
	private Integer quality;
	private Timestamp createtime;
	private Integer state;

	// Constructors

	/** default constructor */
	public OrderId() {
	}

	/** minimal constructor */
	public OrderId(Integer id, Goods goods, UserInfo userInfo, Integer quality,
			Integer state) {
		this.id = id;
		this.goods = goods;
		this.userInfo = userInfo;
		this.quality = quality;
		this.state = state;
	}

	/** full constructor */
	public OrderId(Integer id, Goods goods, UserInfo userInfo, Integer quality,
			Timestamp createtime, Integer state) {
		this.id = id;
		this.goods = goods;
		this.userInfo = userInfo;
		this.quality = quality;
		this.createtime = createtime;
		this.state = state;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public UserInfo getUserInfo() {
		return this.userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public Integer getQuality() {
		return this.quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public Timestamp getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Timestamp createtime) {
		this.createtime = createtime;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof OrderId))
			return false;
		OrderId castOther = (OrderId) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getGoods() == castOther.getGoods()) || (this
						.getGoods() != null && castOther.getGoods() != null && this
						.getGoods().equals(castOther.getGoods())))
				&& ((this.getUserInfo() == castOther.getUserInfo()) || (this
						.getUserInfo() != null
						&& castOther.getUserInfo() != null && this
						.getUserInfo().equals(castOther.getUserInfo())))
				&& ((this.getQuality() == castOther.getQuality()) || (this
						.getQuality() != null && castOther.getQuality() != null && this
						.getQuality().equals(castOther.getQuality())))
				&& ((this.getCreatetime() == castOther.getCreatetime()) || (this
						.getCreatetime() != null
						&& castOther.getCreatetime() != null && this
						.getCreatetime().equals(castOther.getCreatetime())))
				&& ((this.getState() == castOther.getState()) || (this
						.getState() != null && castOther.getState() != null && this
						.getState().equals(castOther.getState())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result
				+ (getGoods() == null ? 0 : this.getGoods().hashCode());
		result = 37 * result
				+ (getUserInfo() == null ? 0 : this.getUserInfo().hashCode());
		result = 37 * result
				+ (getQuality() == null ? 0 : this.getQuality().hashCode());
		result = 37
				* result
				+ (getCreatetime() == null ? 0 : this.getCreatetime()
						.hashCode());
		result = 37 * result
				+ (getState() == null ? 0 : this.getState().hashCode());
		return result;
	}

}