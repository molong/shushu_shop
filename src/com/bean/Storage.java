package com.bean;

import java.sql.Timestamp;

/**
 * Storage entity. @author MyEclipse Persistence Tools
 */

public class Storage implements java.io.Serializable {

	// Fields

	private Integer id;
	private Goods goods;
	private Integer quality;
	private Timestamp updatetime;

	// Constructors

	/** default constructor */
	public Storage() {
	}

	/** minimal constructor */
	public Storage(Integer id, Goods goods, Integer quality) {
		this.id = id;
		this.goods = goods;
		this.quality = quality;
	}

	/** full constructor */
	public Storage(Integer id, Goods goods, Integer quality,
			Timestamp updatetime) {
		this.id = id;
		this.goods = goods;
		this.quality = quality;
		this.updatetime = updatetime;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public Integer getQuality() {
		return this.quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public Timestamp getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}

}